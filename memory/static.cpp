
//Static functions can only call static variables

class Card
{
private:
	static bool aceHigh;
	int value;
public:
	Card() { }
	static bool IsAceHigh() { return aceHigh; }
};

bool Card::aceHigh = false;

int main()
{
	//Card c, d;
	bool high = Card::IsAceHigh();
}
