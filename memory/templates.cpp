#include <iostream>

template <class T>
class vector
{
	T * data;
	int position, size;

public:
	vector() { size = 10; position = 0; data = new T[size]; }
	~vector() { delete [] data; }
	void push_back(T value) { data[position++] = value; }
	T pop_back() { return data[--position]; }
};

/** Write a vector class with a constructor, destructor, push_back and pop_back.
*/
int main()
{
	vector<char> v;
	v.push_back('p');
	v.push_back('k');

	std::cout << v.pop_back() << std::endl;
	std::cout << v.pop_back() << std::endl;
}
