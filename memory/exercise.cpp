#include <iostream>



/** Write a vector class with a constructor, destructor, push_back and pop_back
    that holds 10 elements.
*/
int main()
{
	vector v;
	v.push_back(7);
	v.push_back(6);

	std::cout << v.pop_back() << std::endl << v.pop_back() << std::endl;
}
