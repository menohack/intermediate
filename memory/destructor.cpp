#include <vector>

using namespace std;

//The ONLY difference between struct and class is
//that a struct is default public

class Cat
{
private:
	char * string;
	char string2[20];
	int size;
public:
	Cat() { string = new char[20]; }
	Cat(int size) { string = new char[size]; this->size = size; }
	~Cat() { delete [] string; }
};


int main()
{
	Cat c(30);

	Cat * ptr = new Cat();

	*ptr = c;

	delete ptr;

	return 0;
}

//ptr is no longer valid down here,
//however, what it points to is
