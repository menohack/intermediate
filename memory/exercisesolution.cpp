#include <iostream>

class vector
{
	int * data;
	int position, size;

public:
	vector() { size = 10; position = 0; data = new int[size]; }
	~vector() { delete [] data; }
	void push_back(int value) { data[position++] = value; }
	int pop_back() { return data[--position]; }
};

/** Write a vector class with a constructor, destructor, push_back and pop_back.
*/
int main()
{
	vector v;
	v.push_back(7);
	v.push_back(6);

	std::cout << v.pop_back() << std::endl;
	std::cout << v.pop_back() << std::endl;
}
