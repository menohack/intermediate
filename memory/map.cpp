#include <unordered_map>
#include <string>
#include <iostream>

class elephant
{
	int t;
public:
	elephant() {}
	elephant(int t) { this->t = t; }
	void print() { std::cout << t << std::endl; }
};

//Compile with -std=c++11

int main()
{
	std::unordered_map<std::string, elephant> map;
	map["james"] = elephant(2);
	map["brian"] = elephant(6);

	for (auto iterator = map.begin(); iterator != map.end(); iterator++)
		iterator->second.print();
}
