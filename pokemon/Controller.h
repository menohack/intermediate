class Controller
{
	virtual void HandleInput() = 0;
};
