#include <string>
#include <SDL/SDL.h>

using std::string;

class View
{
	/** Draws the string of text starting at the (x,y) position.
	*/
	virtual void DrawText(string& text, float x, float y) = 0;

	/** Draws the surface at the given location.
	*/
	virtual void DrawImage(SDL_Surface* image, float x, float y) = 0;

	/** Draws a solid rectangle, for the health bar.
	*/
	virtual void DrawRect(float x, float y, float width, float height) = 0;

};
