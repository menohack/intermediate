#include <SDL/SDL.h>
#include <SDL/SDL_image.h>
#include <SDL/SDL_ttf.h>
#include <stdlib.h>

#include "Game.h"
#include "View.h"

void Game::HandleInput()
{
	SDL_Event event;
	while (SDL_PollEvent(&event))
	{
		switch (event.type)
		{
			case SDL_QUIT:
				done = true;
				break;
			case SDL_KEYDOWN:
				if (atTrainerScreen)
					atTrainerScreen = false;
				switch (event.key.keysym.sym) {
					case SDLK_ESCAPE:
						done = true;
						break;
					case SDLK_RETURN:
						if (!atTrainerScreen)
							textBox = !textBox;
					default:
						break;
				}
				break;

			case SDL_KEYUP:
				break;

			default:
				break;	
		}
	}

}

SDL_Surface * LoadImage(const char * filename)
{
	SDL_Surface * temp = IMG_Load(filename);
	if (temp)
	{
		SDL_Surface * optimized = SDL_DisplayFormat(temp);
		SDL_FreeSurface(temp);
		return optimized;
	}

	return 0;
}


Game::Game() : atTrainerScreen(true), done(false), textBox(false)
{
}

int Game::Gameloop()
{
	if (SDL_Init(SDL_INIT_EVERYTHING) < 0)
	{
		printf("Failed to initialize SDL\n");
		return 0;
	}

	atexit(SDL_Quit);

	SDL_Surface * screen = SDL_SetVideoMode(480,432, 32, SDL_HWSURFACE);

	if (!screen)
	{
		printf("Failed to initialize window\n");
		return 0;
	}

	if( TTF_Init() == -1 )
	{
		return false;    
	}

	TTF_Font * font = TTF_OpenFont( "Pokemon GB.ttf", 24 );
	SDL_Color textColor = { 0, 0, 0, 0 };

	SDL_WM_SetCaption("Pokemon", 0);

	unsigned previous = SDL_GetTicks();
	unsigned framerate = 1000/60;

			
	SDL_Surface * trainerScreen = LoadImage("TrainerScreen.png");
	SDL_Surface * bottomBar = LoadImage("BottomBar.png");
	SDL_Surface * bulbasaurBack = LoadImage("BulbasaurBack.png");
	SDL_Surface * charmanderFront = LoadImage("CharmanderFront.png");
	SDL_Surface * cursor = LoadImage("Cursor.png");
	SDL_Surface * topHealthBar = LoadImage("TopHealthbar.png");
	SDL_Surface * bottomHealthBar = LoadImage("BottomHealthbar.png");
	SDL_Surface * textBar = LoadImage("TextBar.png");
	
			
	SDL_Surface * fight = TTF_RenderText_Solid( font, "FIGHT", textColor );
	SDL_Surface * item = TTF_RenderText_Solid( font, "ITEM", textColor );
	SDL_Surface * pkmn = TTF_RenderText_Solid( font, "PKMN", textColor );
	SDL_Surface * run = TTF_RenderText_Solid( font, "RUN", textColor );
	

	unsigned cursorTicks = 0;

	while (!done)
	{
		unsigned now = SDL_GetTicks();

		unsigned delta = now - previous;
		previous = now;
		if (delta < framerate)
			SDL_Delay(framerate - delta);

		
		HandleInput();

		SDL_Rect rect = { 0, 0, 480, 432 };
	
		//Clear the screen to white
		SDL_FillRect(screen, &rect, SDL_MapRGB(screen->format, 255, 255, 255));
		

		if (atTrainerScreen)
		{
			rect = { 0, 0, 0, 0 };
			SDL_BlitSurface(trainerScreen, 0, screen, &rect);
		}
		else
		{
			if (!textBox)
			{
				rect = { 0, (Sint16)(432 - bottomBar->h), 0, 0 };
				SDL_BlitSurface(bottomBar, 0, screen, &rect);
				
				//Menu option text	
				rect = { 240, 339, 0, 0};
				SDL_BlitSurface(fight, 0, screen, &rect);

				rect = { 240, 387, 0, 0};
				SDL_BlitSurface(item, 0, screen, &rect);

				rect = { 384, 339, 0, 0};
				SDL_BlitSurface(pkmn, 0, screen, &rect);

				rect = { 384, 387, 0, 0};
				SDL_BlitSurface(run, 0, screen, &rect);
				
				//Draw the blinking cursor
				cursorTicks += delta;
				
				if (cursorTicks > 1000)
					cursorTicks -= 1000;
				else if (cursorTicks > 500)
				{
					rect = { 220, 335, 0, 0 };
					SDL_BlitSurface(cursor, 0, screen, &rect);
				}
			}
			else
			{
				rect = { 0, (Sint16)(432 - textBar->h), 0, 0 };
				SDL_BlitSurface(textBar, 0, screen, &rect);
			}

			rect = { 20, 175, 0, 0 };
			SDL_BlitSurface(bulbasaurBack, 0, screen, &rect);
			
			rect = { 305, 40, 0, 0 };
			SDL_BlitSurface(charmanderFront, 0, screen, &rect);
		

			//Health bars
			SDL_BlitSurface(topHealthBar, 0, screen, 0);
			
			rect = { 216, 195, 0, 0 };
			SDL_BlitSurface(bottomHealthBar, 0, screen, &rect);
			
			float charmanderHealth = 0.7f, bulbasaurHealth = 1.0f;
			rect = { 96, 57, (Uint16)(144 * charmanderHealth), 6 };
			SDL_FillRect(screen, &rect, SDL_MapRGB(screen->format, 80, 80, 80));
			
			rect = { 288, 225, (Uint16)(144 * bulbasaurHealth), 6 };
			SDL_FillRect(screen, &rect, SDL_MapRGB(screen->format, 80, 80, 80));


		}

		SDL_Flip(screen);
	}
	
	SDL_FreeSurface(trainerScreen);
	SDL_FreeSurface(bottomBar);
	SDL_FreeSurface(bulbasaurBack);
	SDL_FreeSurface(charmanderFront);
	SDL_FreeSurface(cursor);
	SDL_FreeSurface(topHealthBar);
	SDL_FreeSurface(bottomHealthBar);
	SDL_FreeSurface(textBar);
	
	SDL_FreeSurface(fight);
	SDL_FreeSurface(item);
	SDL_FreeSurface(pkmn);
	SDL_FreeSurface(run);

	SDL_FreeSurface(screen);

	TTF_CloseFont(font);
	TTF_Quit();

	SDL_Quit();

	return 1;
}

int main()
{
	Game game;
	return game.Gameloop();
}
