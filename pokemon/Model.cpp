#include <vector>

using std::vector;

enum MenuItem
{
	FIGHT, PKMN, ITEM, RUN, ABILITY1, ABILITY2, ABILITY3, ABILITY4
};

class Pokemon;
class Trainer;
class Ability;

class Game : public Model
{
pubic:
	/** Gets the active Pokemon for a given trainer.
	*/
	virtual Pokemon GetActivePokemon(Trainer& trainer) = 0;

	/** Gets the active trainer.
	*/
	virtual Trainer GetCurrentTurn() = 0;

	/** Gets a vector of the abilities for a given Pokemon.
	*/
	virtual vector<Ability> GetAbilities(Pokemon& pokemon) = 0;
	
	/** Gets the current cursor position.
	*/
	virtual MenuItem GetCursorPosition() = 0;
};
