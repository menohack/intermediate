#include <stdio.h>

class GameObject
{
private:
	static int numObjects;
public:
	GameObject() { numObjects++; }
	~GameObject() { numObjects--; }
	static int GetNumObjects() { return numObjects; }
};

//Need this to declare the space for numObjects and set it to 0
int GameObject::numObjects = 0;

int main()
{
	int i = GameObject::GetNumObjects();
	printf("%d\n", i);
	
	//Calls constructor then destructor
	GameObject();
	
	i = GameObject::GetNumObjects();
	printf("%d\n", i);
	
	//Calls constructor
	GameObject go;

	i = GameObject::GetNumObjects();
	printf("%d\n", i);

	//Destructor is never called
	new GameObject;
	
	i = GameObject::GetNumObjects();
	printf("%d\n", i);
}
//The destructor of go is called here
