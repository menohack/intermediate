#include <stdio.h>
#include <iostream>
#include <vector>
#include <string>
#include <string.h>

using namespace std;

namespace Kingdom
{
	class Animal { public: virtual string GetSound() = 0; };
}

int main()
{
	const int 
	* const * const * const * const * const
	* const * const * const * const * const
	* const * const * const * const * const
	* const * const * const * const * const
	* const * const * const * const * const
	* const * const * const * const * const
	* const * const * const * const * const
	* const * const * const * const * const
	* const * const * const * const * const
	* const * const * const * const * const
	p = 0;

#define AVG(a, b) ((a) + (b))/ 2

	int t = AVG(10, 15);
	printf("AVG(10, 15) = %d\n", t);
	
	double d = AVG(10.0, 15.0);
	printf("AVG(10.0, 15.0) = %f\n", d);


	//Classes can be defined almost anywhere
	class Cat : public Kingdom::Animal { public: string GetSound() { return "Meow"; } };
	class Dog : public Kingdom::Animal { public: string GetSound() { return "Woof"; } };

	//Just another example of dynamic dispatch
	//The Animal pointer will call the correct function
	vector<Kingdom::Animal*> animals;
	animals.push_back(new Cat);
	animals.push_back(new Dog);

	cout << animals[0]->GetSound() << endl;
	cout << animals[1]->GetSound() << endl;

	//This make no sense -- the compiler will stop us
	//static_cast<Dog*>(new Cat);

	//However, the compiler will go along with this happily
	reinterpret_cast<Dog*>(new Cat);

	//Both of these compile -- compiler has no way of knowing animals[1] is a dog
	//It does not check at run-time if it is a valid cast
	Cat * realCat = static_cast<Cat*>(animals[0]);
	Cat * notACat = static_cast<Cat*>(animals[1]);
	cout << "realCat: " << realCat << ", notACat: " << notACat << endl;

	//With dynamic_cast notACat is null because it is checked at run-time
	realCat = dynamic_cast<Cat*>(animals[0]);
	notACat = dynamic_cast<Cat*>(animals[1]);
	cout << "realCat: " << realCat << ", notACat: " << notACat << endl;


	//c is and always shall point to 0!
	const char * constString = "this is constant";

	//Modifying a constant variable results in undefined behavior
	char * nope = const_cast<char*>(constString);
	//This segfaults
	//strcpy(nope, "not anymore!");


	char notConst[] = "the one ring";
	const char * donottouch = const_cast<const char*>(notConst);

	//Now we can give our precious string to an untrusted function:

	//smeagol(donottouch);


}
