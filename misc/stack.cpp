#include <stdio.h>

//Claims memory on the stack and fills it with zeros
void SmashStack()
{
	int data[200];
	for (int i=0; i < 200; i++)
		data[i] = 0;
}

int * MakeArray()
{
	int array[] = { 42, 6, -1, 192 };

	return array;
}


/** MakeArray will work because the values on the stack
    have not been changed. However, when we call SmashStack
    that function now gets access to the memory previously
    used by MakeArray. Once SmashStack returns that memory
    is "free", but still initialized to the last value.
*/
int main()
{
	int * p = MakeArray();

	//SmashStack();

	printf("%d %d %d %d\n", p[0], p[1], p[2], p[3]);
}
