#include <vector>
#include <iostream>

using namespace std;

struct GameObject {};


/** This is called a Singleton class. It is very useful
    when a class will only exist once.
*/
class World
{
private:
	static World* world;

	vector<GameObject*> gameObjects;

	//Private constructor!
	World() { gameObjects.push_back(new GameObject()); }
public:
	static World* GetInstance() { world = new World(); return world; }

	vector<GameObject*>& GetGameObjects() { return gameObjects; }
};



/** Solve this very subtle problem to make the program compile.
*/
int main()
{
	World * world = World::GetInstance();

	vector<GameObject*> gameObjects = world->GetGameObjects();
	cout << "World has " << gameObjects.size() << " GameObjects" << endl;
}
