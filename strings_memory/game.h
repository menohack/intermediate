#ifndef _GAME_H
#define _GAME_H

//define statements literally replace the token with its value
//before the compiler runs. Typing NUM_PLAYERS is like typing 2.
//Statements like NUM_PLAYERS++ makes no sense (2++ makes no sense either)
#define NUM_PLAYERS 2
#define FIREBALL_DAMAGE 30
#define ARROW_DAMAGE 40

int getPlayerIndex(const char * name);
bool gameOver();
bool isAlive(int);
void fireball(int, int);
void arrow(int, int);
char* getPlayerName(int);
void checkValidPlayer(int);
void printTurn();

#endif //_GAME_H
