#ifndef _EXERCISE1_H
#define _EXERCISE1_H

/** Returns the length of the string, not counting the null character.
*/
int stringlength(const char * string);

/** Copies source into destination, including the null character. Returns destination.
*/
char * stringcopy(char * destination, const char * source);

#endif //_EXERCISE1_H
