#include <stdio.h>

#include "exercise1.h"

int main(int argc, char * argv[])
{
	if (argc != 3)
	{
		printf("Usage: exercise1 <string1> <string2>\n");
		return 0;
	}

	int length1 = stringlength(argv[1]);
	int length2 = stringlength(argv[2]);

	printf("first: %s, length: %d. second: %s, length: %d\n", argv[1], length1,
		argv[2], length2);

	char string1[10], string2[10];
	if (length1 < 10 && length1 >= 0 && length2 < 10 && length2 >= 0)
	{
		stringcopy(string1, argv[1]);
		stringcopy(string2, argv[2]);

		printf("Our new strings: %s %s\n", string1, string2);
	}
	else
		printf("Strings are too large! If only we had some type of dynamic memory...\n");


	return 0;
}


/** Returns the length of the string, not counting the null character.
*/
int stringlength(const char * string)
{
	
	int position = 0;
	//while (string[position] != '\0')
	while (*(string + position) != '\0')
		position++;
	
	return position;
}

char * stringcopy(char * destination, const char * source)
{
	int position = 0;
	while ((destination[position] = source[position]) != '\0')
		position++;

	return destination;
}



