#include <assert.h>
#include <stdbool.h>
#include <stdio.h>
#include <string.h>
#include <stdlib.h>

#include "game.h"

//Global variables
int health[] = { 100, 100 };
char * players[NUM_PLAYERS];

//Enums are integers
//PLAYER1 == 0, PLAYER2 == 1
enum
{
	PLAYER1, PLAYER2
}; //Don't forget the semi-colon!

int main(int argc, char * argv[])
{
	//Make sure two strings are specified
	if (argc != 3)
	{
		printf("Usage: game <player1> <player2>\n");
		return 0;
	}

	//We need to allocate memory before strcpy
	players[0] = (char*) malloc(sizeof(char) * (strlen(argv[1]) + 1));
	players[1] = (char*) malloc(sizeof(char) * (strlen(argv[2]) + 1));
	
	strcpy(players[0], argv[1]);
	strcpy(players[1], argv[2]);

	int turn = 0;
	while (true)
	{
		if (turn >= NUM_PLAYERS)
			turn = 0;

		printTurn(turn);
		if (gameOver())
			break;
	
		//Attack and target tokens longer than 9 characters
		//will not work	
		char attack[10], target[10];
		scanf("%s %s", attack, target);

		int playerIndex = getPlayerIndex(target);

		if (strcmp(attack, "fireball") == 0)
			fireball(turn, playerIndex);
		else if (strcmp(attack, "arrow") == 0)
			arrow(turn, playerIndex);
		else
			printf("Invalid spell!\n");

		turn++;
	}
	
	free(players[0]);
	free(players[1]);

	return 0;
}

int getPlayerIndex(const char * name)
{
	for (int i=0; i < NUM_PLAYERS; i++)
		if (strcmp(name, players[i]) == 0)
			return i;

	//We haven't found the name!
	printf("Invalid target! Type a new player name:\n");

	char newName[10];
	scanf("%s", newName);

	return getPlayerIndex(newName);
}

bool gameOver()
{
	for (int i=0; i < NUM_PLAYERS; i++)
		if (!isAlive(i))
			return true;

	return false;
}

bool isAlive(int player)
{
	checkValidPlayer(player);
	return health[player] > 0;
}

void printTurn(int player)
{
	printf("%s now has %d health", getPlayerName(0), health[0]);
	for (int i=1; i < NUM_PLAYERS; i++)
		printf(", %s now has %d health", getPlayerName(i), health[i]);
	printf("\n%s's turn\n", getPlayerName(player));
}

void fireball(int source, int target)
{
	checkValidPlayer(source);
	checkValidPlayer(target);

	//Decrease target's health
	health[target] -= FIREBALL_DAMAGE;

	printf("%s cast fireball at %s, dealing %d damage\n", getPlayerName(source),
		 getPlayerName(target), FIREBALL_DAMAGE);
}

void arrow(int source, int target)
{
	checkValidPlayer(source);
	checkValidPlayer(target);

	//Decrease target's health
	health[target] -= ARROW_DAMAGE;

	printf("%s shot an arrow at %s, dealing %d damage\n", getPlayerName(source),
		 getPlayerName(target), ARROW_DAMAGE);
}

/** Returns the player's name from the array of names base on his or her index.
*/
char * getPlayerName(int player)
{
	checkValidPlayer(player);
	return players[player];
}

/** Checks if the player (integer) is valid. Exits the program on failure.
*/
void checkValidPlayer(int player)
{
	assert(player >= 0);
	assert(player < NUM_PLAYERS);
}
