#include <ctype.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#include "exercise2.h"

int main(int argc, char * argv[])
{
	if (argc != 2)
	{
		printf("Usage: exercise2 <string>\n");
		printf("Switches upper-case and lower-case characters.\n");
		printf("Example: Hello produces hELLO\n");
		return 0;
	}

	//Create a new string identical to the second argument
	char * duplicate = stringduplicate(argv[1]);

	//Swap the upper-case and lower-case letters
	swapcases(duplicate);

	//Print the original string and the new string
	printf("%s -> %s\n", argv[1], duplicate);
	
	//Don't forget to free the new memory!
	free(duplicate);
	return 0;
}

/** Returns a pointer to a new string which is a duplicate of string. The returned pointer
can be passed to free().
*/
char * stringduplicate(const char * string)
{
	int length = strlen(string) + 1;

	char * result = malloc(sizeof(char) * length);
	
	strcpy(result, string);

	return result;
}

/** Swaps upper-case and lower-case letters. Numeric characters and symbols stay the same.
HINT: Use isupper(char), islower(char), toupper(char), and tolower(char)
*/
void swapcases(char * string)
{
	int pos = 0;
	while (string[pos] != '\0')
	{
		if (isupper(string[pos]))
			string[pos] = tolower(string[pos]);
		else if (islower(string[pos]))
			string[pos] = toupper(string[pos]);

		pos++;
	}
}


