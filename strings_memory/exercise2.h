#ifndef _EXERCISE2_H
#define _EXERCISE2_H

/** Returns a pointer to a new string which is a duplicate of string. The returned pointer
can be passed to free().
*/
char * stringduplicate(const char * string);

/** Swaps upper-case and lower-case letters. Numeric characters and symbols stay the same.
HINT: Use isupper(char), islower(char), toupper(char), and tolower(char)
*/
void swapcases(char * string);

#endif //_EXERCISE2_H
