#include <map>
#include <string>

using namespace std;

class GameObject
{
protected:
	int x, y;
public:
	GameObject(int x, int y) { this->x = x; this->y = y; }
	int GetX() { return x; }
	int GetY() { return y; }
	virtual string GetType() { return "GameObject"; }
};

class Player : public GameObject
{
public:
	Player(int x, int y) : GameObject(x, y) {}
	string GetType() { return "Player"; }
};

class NPC : public GameObject
{
public:
	NPC(int x, int y) : GameObject(x, y) {}
	string GetType() { return "NPC"; }
};

/** Implement derived classes Player and NPC.
*/
int main()
{
	map<string, GameObject*> gameObjects;

	gameObjects["rock"] = new GameObject(15, 15);
	gameObjects["James"] = new Player(10, 10);
	gameObjects["Balthazaar"] = new NPC(0, 20);


	//Compile with -std=c++0x to allow for-each loops and auto
	for (auto g : gameObjects)
		printf("%s %s is at (%d, %d)\n", g.second->GetType().c_str(), g.first.c_str(), g.second->GetX(), g.second->GetY());

	return 0;
}
