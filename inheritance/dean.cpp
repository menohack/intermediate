#include <stdio.h>
#include <vector>

class Professor
{
protected:
	int money;
public:
	Professor() : money(0) {}
	virtual void GetPaid(int money) 
	{ 
		this->money += money;
		printf("Professor was paid $%d\n", this->money);
	}
};

class Dean : public Professor
{
public:
	Dean() : Professor() {}
	
	void GetPaid(int money) 
	{ 
		this->money += 2*money;
		printf("Professor was paid $%d\n", this->money);
	}
	void DeanParty() { printf("Deans had a party\n"); }
};

using namespace std;

void func(vector<Professor*> profs)
{
	// Dynamic dispatch will use the pointer in the Professor
	// object to determine which GetPaid to call
	profs[0]->GetPaid(100);
	profs[1]->GetPaid(100);
}


int main()
{
	Dean boswell;
	// Calling GetPaid from the base class using the scope operator
	//boswell.Professor::GetPaid(100);
	
	// Calls GetPaid from derived class due to overload
	//boswell.GetPaid(100);

	vector<Professor*> profs;
	profs.push_back(new Professor());
	profs.push_back(new Dean());

	// To turn profs[1] back into a Dean
	Dean * d1 = dynamic_cast<Dean*>(profs[1]);
	
	// Now we can call Dean functions that don't exist in Professor
	d1->DeanParty();

	// dynamic_cast will return a null pointer if the conversion
	// is impossible
	Dean * d2 = dynamic_cast<Dean*>(profs[0]);
	if (d2 == 0)
		printf("profs[0] was not a Dean\n");
}
