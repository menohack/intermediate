#include <stdio.h>

class CharacterClass
{
protected:
	int health, mana;
	bool stunned;
public:
	CharacterClass(int health, int mana) : health(health), mana(mana) {}

	virtual void DealDamage(int damage) = 0;
	virtual bool IsAlive() = 0;
	
	virtual void Stun() = 0;
	virtual bool IsStunned() = 0;
};

class Mage : public CharacterClass
{
public:
	Mage(int health, int mana) : CharacterClass(health, mana) {}

	void DealDamage(int damage) { health -= damage; }
	bool IsAlive() { return health > 0; }

	void Stun() { stunned = true; }
	bool IsStunned() { return stunned; }
};

/** Write the Mage class which implements the CharacterClass interface.
*/
int main()
{
	CharacterClass * mage = new Mage(100, 70);

	mage->DealDamage(40);
	printf("Dealt 40 damage\n");
	if (!mage->IsAlive())
		printf("Mage has died from only 40 damage!\n");

	mage->Stun();
	if (mage->IsStunned())
		printf("Stunned\n");
	else
		printf("Mage should have been stunned!\n");

	mage->DealDamage(70);
	printf("Dealth 70 damage\n");
	if (!mage->IsAlive())
		printf("Mage has died\n");
	else
		printf("Mage should have died from 110 damage!\n");

	return 0;
}
