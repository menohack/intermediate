class Rectangle
{
protected:
	int w, h;
public:
	int Area() { return w*h; }
};

// Square IS-A Rectangle
class Square : public Rectangle
{
public:
	int GetWidth() { return w; }
};

int main()
{
	Square s;
	s.Area();
	s.GetWidth();
}
