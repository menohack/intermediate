#include <stdio.h>

class Rectangle
{
protected:
	int w, h;
public:
	Rectangle(int w, int h) : w(w), h(h) {}
	// virtual means this function can be overloaded
	virtual int Area() { return w*h; }
};

class Square : public Rectangle
{
public:
	Square(int side) : Rectangle(side, 0) {}
	int Area() { return w*w; }

};

int main()
{
	// This compiles but is very bad
	// The size in memory of Rectangle and Square may not match
	//Rectangle r = Square(4);

	// r is secretly a Square, but we treat it like a Rectangle
	Rectangle * r = new Square(4);

}
