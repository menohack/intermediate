#include <stdio.h>

int main()
{
	//fopen(, "r") can tell you if a file exists
	FILE * file = fopen("file", "r+");

	char hello[] = "hello";
	
	fseek(file, 0, SEEK_END);

	int size = ftell(file);

	printf("Size of file is %d\n", size);
	
	fwrite(hello, sizeof(hello), 1, file);
	
	fclose(file);
}
