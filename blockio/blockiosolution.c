#include <stdlib.h>
#include <stdio.h>

/** Creates a new file named output identical to the file named input.
*/
void copyfile(const char * input, const char * output)
{
	FILE * in = fopen(input, "r");
	FILE * out = fopen(output, "w");
	
	fseek(in, 0, SEEK_END);
	int size = ftell(in);

	fseek(in, 0, SEEK_SET);
	
	char * data = malloc(size);
	fread(data, size, 1, in);
	
	fwrite(data, size, 1, out);

	free(data);

	fclose(in);
	fclose(out);
}

int main()
{
	copyfile("exercise.c", "copied.c");

	return 0;
}
