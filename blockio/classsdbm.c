#include <stdio.h>
#include <string.h>

#include "sdbm.h"

#define DEBUG 1

enum
{
	NO_ERROR,
	DATABASE_NOT_FOUND
};

int error = NO_ERROR;

/**
 * Create new database with given name. You still have
 * to sdbm_open() the database to access it. Return true
 * on success, false on failure.
 */
bool sdbm_create(const char *name)
{
#ifdef DEBUG
	printf("SDBM: Create\n");
#endif
	return true;
}

/**
 * Open existing database with given name. Return true on
 * success, false on failure.
 */
bool sdbm_open(const char *name)
{
#ifdef DEBUG
	printf("SDBM: Open\n");
#endif
	/* Example
	FILE * file = fopen(...);
	if (!file)
	{
		error = DATABASE_NOT_FOUND;
		return false;
	}
	*/
	return true;
}

/**
 * Synchronize all changes in database (if any) to disk.
 * Useful if implementation caches intermediate results
 * in memory instead of writing them to disk directly.
 * Return true on success, false on failure.
 */
bool sdbm_sync()
{
	return false;
}

/**
 * Close database, synchronizing changes (if any). Return
 * true on success, false on failure.
 */
bool sdbm_close()
{
	return false;
}

/**
 * Return error code for last failed database operation.
 */
int sdbm_error()
{
	int temp = error;
	error = NO_ERROR;
	return temp;
}

/**
 * Is given key in database?
 */
bool sdbm_has(const char *key)
{
#ifdef DEBUG
	printf("SDBM: Has %s\n", key);
#endif
	if (strcmp(key, "james") == 0)
		return true;
	else
		return false;
}

/**
 * Get value associated with given key in database.
 * Return true on success, false on failure.
 *
 * Precondition: sdbm_has(key)
 */
bool sdbm_get(const char *key, Player *value)
{
#ifdef DEBUG
	printf("SDBM: Get %s\n", key);
#endif
	Player james = { "james", 0, 100, 80, 10000, 10.0f, 0.0f };
	if (sdbm_has(key))
	{
		*value = james;
		return true;
	}
	return false;
}

/**
 * Update value associated with given key in database
 * to given value. Return true on success, false on
 * failure.
 *
 * Precondition: sdbm_has(key)
 */
bool sdbm_put(const char *key, const Player *value)
{
	return false;
}

/**
 * Insert given key and value into database as a new
 * association. Return true on success, false on
 * failure.
 *
 * Precondition: !sdbm_has(key)
 */
bool sdbm_insert(const char *key, const Player *value)
{
#ifdef DEBUG
	printf("SDBM: Insert %s\n", key);
#endif
	if (sdbm_has(key))
		return false;
	else
		return true;
}

/**
 * Remove given key and associated value from database.
 * Return true on success, false on failure.
 *
 * Precondition: sdbm_has(key)
 */
bool sdbm_remove(const char *key)
{
	return false;
}

