#include <stdio.h>

int main()
{
	FILE * file = fopen("output.txt", "w");
	//Check if fopen worked
	if (file == 0)
		return 0;
	
	printf("What is your name?\n");

	char name[32];
	scanf("%s", name);

	printf("What is your age?\n");

	int age;
	scanf("%d", &age);

	if ( age >= 21 )
		fprintf(file, "Hello %s, you are old enough to drink\n", name);
	else
		fprintf(file, "Hello %s, you are NOT old enough to drink\n", name);

	fclose(file);
}
