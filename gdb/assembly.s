	.file	"asm.c"
	.section	.rodata
.LC0:
	.string	"number = %c\n"
	.text
	.globl	main
	.type	main, @function
main:
.LFB0:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	andl	$-16, %esp
	subl	$32, %esp

	#Put 2 into memory
	movb	$2, 29(%esp)

	#Put 5 into memory
	movb	$5, 30(%esp)

	#Moves both values into the registers edx and eax
	movzbl	29(%esp), %edx
	movzbl	30(%esp), %eax
	
	#Add 2 + 5
	addl	%edx, %eax

	#Do some stuff...
	movb	%al, 31(%esp)
	movsbl	31(%esp), %eax
	movl	%eax, 4(%esp)
	movl	$.LC0, (%esp)


	#Call printf
	call	printf

	leave
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
.LFE0:
	.size	main, .-main
	.ident	"GCC: (Ubuntu/Linaro 4.7.2-2ubuntu1) 4.7.2"
	.section	.note.GNU-stack,"",@progbits
