#include <stdio.h>

/** Sums size values from array and returns the sum.
*/
int Sum(char array[], int size)
{
	int result = 0;
	for (int i=0; i < size; i++)
		result += array[i];
	return result;
}

int main()
{
	unsigned char array[] = { 70, 4, 200, 16, 99 };
	
	//sizeof(array) gives you the number of bytes allocated for array
	//ONLY IF array is an array and not a pointer (no malloc)
	int result = Sum(array, sizeof(array) / sizeof(array[0]));
	printf("%d\n", result);
}
