	.file	"local.c"
	.globl	pi
	.section	.rodata
	.align 4
	.type	pi, @object
	.size	pi, 4
pi:
	.long	1078530000
	.globl	numPlayers
	.data
	.align 4
	.type	numPlayers, @object
	.size	numPlayers, 8
numPlayers:
	.long	1
	.long	2
	.text
	.globl	Average
	.type	Average, @function
Average:
.LFB0:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	subl	$16, %esp
	movl	12(%ebp), %eax
	movl	8(%ebp), %edx
	addl	%edx, %eax
	movl	%eax, -4(%ebp)
	movl	-4(%ebp), %eax
	movl	%eax, %edx
	shrl	$31, %edx
	addl	%edx, %eax
	sarl	%eax
	leave
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
.LFE0:
	.size	Average, .-Average
	.section	.rodata
.LC0:
	.string	"%f %d\n"
	.text
	.globl	main
	.type	main, @function
main:
.LFB1:
	.cfi_startproc
	pushl	%ebp
	.cfi_def_cfa_offset 8
	.cfi_offset 5, -8
	movl	%esp, %ebp
	.cfi_def_cfa_register 5
	andl	$-16, %esp
	subl	$32, %esp
	movl	$4, (%esp)
	call	malloc
	movl	%eax, 28(%esp)
	movl	$4, 4(%esp)
	movl	$2, (%esp)
	call	Average
	flds	pi
	movl	%eax, 12(%esp)
	fstpl	4(%esp)
	movl	$.LC0, (%esp)
	call	printf
	leave
	.cfi_restore 5
	.cfi_def_cfa 4, 4
	ret
	.cfi_endproc
.LFE1:
	.size	main, .-main
	.ident	"GCC: (Ubuntu/Linaro 4.7.2-2ubuntu1) 4.7.2"
	.section	.note.GNU-stack,"",@progbits
