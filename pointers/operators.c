#include <stdio.h>

void badswap(int a, int b)
{
	int temp = a;
	a = b;
	b = temp;
}

void swap(int * a, int * b)
{
	int temp = *a;
	*a = *b;
	*b = temp;
}

//Forward declare functions that are defined later
void pickLargest(int *, int *, int **);

int main()
{
	//It is undefined where this points
	//int * ptr;

	//Now it points to 0
	int * ptr = 0;

	int seven = 7;

	ptr = &seven;

	//%p means print address of a pointer

	//void * means pointer with no type
	printf("Value is: %d and address is %p\n", *ptr, (void*)ptr);

	int a = 2, b = 6;

	//Pass by value -- changes to a and be do not persist
	badswap(a, b);
	printf("badswap a: %d, b: %d\n", a, b);
	
	//Pass by pointer (reference)
	swap(&a, &b);
	printf("swap a: %d, b: %d\n", a, b);

	//array1 has 4 bytes to store the address and 16 bytes to store the data
	int array1[] = { 3, 9, 4, 2 };
	int array2[] = { 5, 1, 3 };

	//"Basically equivalent"
	//int * array1;
	int * ptrtoarray = array1;
	printf("array1: %p, ptrtoarray: %p\n", array1, ptrtoarray);

	int * result = 0;

	pickLargest(array1, array2, &result);
	printf("array1: %p, array2: %p, result: %p\n", array1, array2, result);
}


/** Chooses the array with the largest first element and puts the address
in result.
*/
void pickLargest(int * array1, int * array2, int ** result)
{
	//&array1 is the same as &array1[0]
	
	if (array1[0] > array2[0])
		*result = array1;
	else
		*result = array2;
}


