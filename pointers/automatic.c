//On the second call of automatic, the stack pointer is 896

void automatic()
{
	//If the stack pointers at 1000
	//after this function is called it is at 948
	int t, a, x;

	//40 bytes
	int local[10];

	automatic();

	//Once this function returns the stack pointer increases by 52
	return;
}

//Local memory is only valid within scope (within {} brackets)
int * getLocalMemory(int x)
{
	//Arrays are declared statically -- you have to know the size 10
	int local[10];


	int dynamic[x];	//Not possible -- compiler error
	//You need dynamic memory for sizes you don't know at compile time

	{
		int t = 2;
	}
	//t is not valid out here

	return local;	//Bad! You cannot return local memory
}






