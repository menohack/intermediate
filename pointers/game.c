#include <assert.h>
#include <stdbool.h>
#include <stdio.h>

#define NUM_PLAYERS 2
#define FIREBALL_DAMAGE 30
#define ARROW_DAMAGE 40

//Global variables
int health[] = { 100, 100 };
char* players[] = { "PLAYER1", "PLAYER2" };

//Forward declarations
bool gameOver();
bool isAlive(int);
void fireball(int, int);
void arrow(int, int);
char* getPlayerName(int);
void checkValidPlayer(int);
void printHealth();

//Enums are integers
//PLAYER1 == 0, PLAYER2 == 1
enum
{
	PLAYER1, PLAYER2
};

int main()
{
	
	while (true)
	{
		printHealth();
		if (gameOver())
			break;
		
		fireball(PLAYER1, PLAYER2);
		arrow(PLAYER2, PLAYER1);
	}
	
	

	return 0;
}

bool gameOver()
{
	for (int i=0; i < NUM_PLAYERS; i++)
		if (!isAlive(i))
			return true;

	return false;
}

bool isAlive(int player)
{
	checkValidPlayer(player);
	return health[player] > 0;
}

void printHealth()
{
	printf("%s now has %d health", getPlayerName(0), health[0]);
	for (int i=1; i < NUM_PLAYERS; i++)
		printf("%s now has %d health", getPlayerName(i), health[i]);
	printf("\n");
}

void fireball(int source, int target)
{
	checkValidPlayer(source);
	checkValidPlayer(target);

	//Decrease target's health
	health[target] -= FIREBALL_DAMAGE;

	printf("%s cast fireball at %s, dealing %d damage\n", getPlayerName(source),
		 getPlayerName(target), FIREBALL_DAMAGE);
}

void arrow(int source, int target)
{
	checkValidPlayer(source);
	checkValidPlayer(target);

	//Decrease target's health
	health[target] -= ARROW_DAMAGE;

	printf("%s shot an arrow at %s, dealing %d damage\n", getPlayerName(source),
		 getPlayerName(target), ARROW_DAMAGE);
}

char * getPlayerName(int player)
{
	checkValidPlayer(player);
	return players[player];
}

void checkValidPlayer(int player)
{
	assert(player >= 0);
	assert(player < NUM_PLAYERS);
}
