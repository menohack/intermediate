#include <stdio.h>
#include <time.h>
#include <stdlib.h>

/** Swaps the values in a and b.
*/
void swap(int * a, int * b)
{
	int temp = *a;
	*a = *b;
	*b = temp;
}

/** Sorts the values in a, b, and c from least to greatest.
*/
void sort(int * a, int * b, int * c)
{
	if (*a > *b)
		swap(a, b);

	if (*b > *c)
	{
		if (*a > *c)
		{
			swap(a, c);
			swap(b,c);
		}
		else
			swap(b, c);
	}
}

/** Returns a pointer to the smallest integer.
*/
int * min(int * integers, int count)
{
	int * minimum = integers;
	for (int i=1; i < count; i++)
		if (integers[i] < *minimum)
			minimum = &integers[i];

	return minimum;
}

int main()
{
	srand(time(0));
	int a = rand() % 20;
	int b = rand() % 20;
	int c = rand() % 20;

	printf("Unsorted: %d %d %d\n", a, b, c);

	int der[3] = { a, b, c };
	//der[0] = a;
	//der[1] = b;
	//der[2] = c;
	int * minimum = min(der, 3);
	
	printf("min: %d\n", *minimum);
	
	sort(&a, &b, &c);

	printf("Unsorted: %d %d %d\n", a, b, c);
}
