#include <stdio.h>
#include <time.h>
#include <stdlib.h>

/** Swaps the values in a and b.
*/
void swap(int * a, int * b)
{
}

/** Sorts the values in a, b, and c from least to greatest.
*/
void sort(int * a, int * b, int * c)
{
}

/** Returns a pointer to the smallest integer.
*/
int * min(int * integers, int count)
{
	return 0;
}

int main()
{
	srand(time(0));
	int a = rand() % 20;
	int b = rand() % 20;
	int c = rand() % 20;

	printf("Unsorted: %d %d %d\n", a, b, c);

	int integers[3] = { a, b, c };
	int * minimum = min(integers, 3);

	if (minimum != 0)	
		printf("min: %d\n", *minimum);
	
	sort(&a, &b, &c);

	printf("Sorted:   %d %d %d\n", a, b, c);
}
