#include <stdio.h>

int main()
{
	int array[] = {2, 4, 6};

	printf("array[1] = %d, array + 1 = %d, array = %p, &array[1] = %p, array + 1 = %p\n", array[1], *(array + 1), array, &array[1], array + 1); 

	int pixels[4][4];
	int count = 0;
//	for (int i=0; i < 4; i++)
//		for (int j=0; j < 4; j++)
//			pixels[j][i] = count++;

	//dont do this pixels[][] anywhere
	
//	count = 0;
//	for (int i=0; i < 16; i++)
//		*(pixels[0] + i) = count++;
	

	count = 0;
	int * ptr = pixels[0];
	for (int i=0; i < 16; i++)
		//These lines are equivalent
		//*(ptr + i) = count++;
		ptr[i] = count++;

}
